#!/usr/bin/python
# -*- encoding: utf-8 -*-

from rest_framework import serializers
from biblioteca.models import *


class PessoaSerializer(serializers.ModelSerializer):
    """
    Classe responsavel por serializer o modelo Pessoa
    """
    class Meta:
        model = Pessoa
        fields = ('id', 'nome')


class LivroSerializer(serializers.ModelSerializer):
    """
    Classe responsavel por serializar o modelo Livro
    """
    class Meta:
        model = Livro
        fields = ('id', 'nome')


class EmprestimoSerializer(serializers.ModelSerializer):
    """
    Classe responsavel por serializer o modelo Emprestimo
    """
    class Meta:
        model = Emprestimo
        fields = ('id', 'user', 'pessoa', 'livro')

