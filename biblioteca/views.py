from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from biblioteca.models import Pessoa, Emprestimo, Livro
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer


# Create your views here.


class PessoaApiView(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'pessoa_list.html'

    def get(self, request, format=None):
        queryset = Pessoa.objects.all()
        return Response({'pessoas': queryset})

    def post(self, request, pk=False):
        try:
            if request.POST.get('nome'):
                pessoa = Pessoa()
                pessoa.nome = request.POST.get('nome')
                pessoa.save()
                return redirect('pessoa')
            else:
                queryset = Pessoa.objects.all()
                return Response({'pessoas': queryset})
        except:
            queryset = Pessoa.objects.all()
            return Response({'pessoas': queryset})


class LivroApiView(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'livro_list.html'

    def get(self, request, format=None):
        queryset = Livro.objects.all()
        return Response({'livros': queryset})

    def post(self, request, pk=False):
        try:
            if request.POST.get('nome'):
                livro = Livro()
                livro.nome = request.POST.get('nome')
                livro.save()
                return redirect('livro')
            else:
                queryset = Livro.objects.all()
                return Response({'livros': queryset})
        except:
            queryset = Livro.objects.all()
            return Response({'livros': queryset})


class EmprestimoApiView(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'emprestimo_list.html'

    def get(self, request, format=None):
        queryset = Emprestimo.objects.all()
        pessoas = Pessoa.objects.all()
        livros = Livro.objects.all()
        usuarios = User.objects.all()
        return Response(
            {'emprestimos': queryset, 'pessoas': pessoas, 'livros': livros, 'usuarios': usuarios})

    def post(self, request, pk=False):
        try:
            if request.POST.get('pk'):
                emprestimo = Emprestimo.objects.get(pk=request.POST.get('pk'))
                emprestimo.livro.emprestado = False
                emprestimo.devolvido = True
                emprestimo.livro.save()
                emprestimo.save()
                return redirect('emprestimo')
            emprestimo = Emprestimo()
            emprestimo.user = User.objects.get(pk=request.POST.get('usuario'))
            emprestimo.livro = Livro.objects.get(pk=request.POST.get('livro'))
            emprestimo.livro.emprestado = True
            emprestimo.livro.save()
            emprestimo.pessoa = Pessoa.objects.get(pk=request.POST.get('pessoa'))
            emprestimo.save()
            return redirect('emprestimo')
        except:
            queryset = Emprestimo.objects.all()
            pessoas = Pessoa.objects.all()
            livros = Livro.objects.all()
            usuarios = User.objects.all()
            return Response(
                {'emprestimos': queryset, 'pessoas': pessoas, 'livros': livros,
                 'usuarios': usuarios})
