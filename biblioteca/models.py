from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Livro(models.Model):
    """
    Classe responsavel por manter os livro
    """
    nome = models.CharField(verbose_name='Nome', max_length=150, blank=False, null=False)
    emprestado = models.BooleanField(verbose_name='Emprestado', default=False)

    def __str__(self):
        return self.nome

    def __unicode__(self):
        return self.nome

class Pessoa(models.Model):
    """
    Classe responsavel manter as pessoas
    """
    nome = models.CharField(verbose_name='Nome', max_length=150, blank=False, null=False)

    def __str__(self):
        return self.nome

    def __unicode__(self):
        return self.nome


class Emprestimo(models.Model):
    """
    Classe responsavel por manter os emprestimos
    """
    user = models.ForeignKey(User, verbose_name='Usuario')
    pessoa = models.ForeignKey(Pessoa, verbose_name='Emprestimo para')
    livro = models.ForeignKey(Livro, verbose_name='Pegou o livro: ')
    devolvido = models.BooleanField(verbose_name='Devolvido', default=False)

    def __str__(self):
        return self.user

    def __unicode__(self):
        return self.user