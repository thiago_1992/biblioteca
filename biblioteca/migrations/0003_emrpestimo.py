# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-18 16:24
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('biblioteca', '0002_auto_20170818_1615'),
    ]

    operations = [
        migrations.CreateModel(
            name='Emrpestimo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('livro', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='biblioteca.Livro', verbose_name='Pegou o livro: ')),
                ('pessoal', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='biblioteca.Pessoa', verbose_name='Emprestimo para')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Usuario')),
            ],
        ),
    ]
