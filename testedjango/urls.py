"""testedjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from biblioteca import views
from django.views.static import serve
from django.conf import settings

router = routers.DefaultRouter()
# router.register(r'v1/pessoa', views.PessoaApiView.as_view(), base_name='pessoa')
# router.register(r'v1/livro', views.LivroApiView)
# router.register(r'v1/emprestimo', views.EmprestimoApiView)


urlpatterns = [
    url(r'^$', views.EmprestimoApiView.as_view(), name='emprestimo'),
    url(r'^admin/', admin.site.urls),
    url(r'^livro/', views.LivroApiView.as_view(), name='livro'),
    url(r'^pessoa/', views.PessoaApiView.as_view(), name='pessoa'),
    url(r'^emprestimo/', views.EmprestimoApiView.as_view(), name='emprestimo'),
    url(r'^emprestimo/(?P<pk>\d+)/', views.EmprestimoApiView.as_view(), name='emprestimo'),
    url(r'^media/(.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
]
